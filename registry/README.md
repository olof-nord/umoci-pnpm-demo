# docker registry

This docker-compose includes a local docker registry.

## Initialise

Note that the registry needs certificates to be generated first, as they are not added to git. See the main readme
and [Makefile](../Makefile#L15).

## Access

Access the web-UI of the local registry at
[http://localhost:8081/repositories](http://localhost:8081/repositories).

The registry is empty on the first start. Pushed images are stored in a docker
volume.

### Authenticate

To authenticate docker to use the local repository, log in to the local docker registry first:

```shell
docker login localhost:5000
```

Username: user
Password: password
