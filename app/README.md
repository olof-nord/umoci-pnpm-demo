# example app

This is basically the simplest JavaScript app possible, point being that it uses a npm library and prints some message
to the console.

Using a npm library means there is a need to download it during build, simulating an actual app.
