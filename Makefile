SHELL := bash

all: build
init: certs start login

IMAGE := umoci-pnpm-demo
TAG := latest
REGISTRY := localhost:5000

ifeq ($(OS), Windows_NT)
	SKOPEO := $(USERPROFILE)\programs\skopeo\skopeo.exe
else
	SKOPEO := $(shell which skopeo 2>/dev/null)
	# use self-signed certificate with skopeo
    # this is used when logging in to the local docker registry
    export SSL_CERT_FILE=registry/certs/domain.crt
endif

.PHONY: certs
certs:
	@echo "Generating certificates..."
	mkdir -p registry/certs
	# this is inspired by the skopeo "systemtest"
	# see https://github.com/containers/skopeo/blob/main/systemtest/helpers.bash#L335
	openssl req -newkey rsa:4096 -nodes -sha256 \
    	-keyout registry/certs/domain.key -x509 -days 365 \
        -out registry/certs/domain.crt \
        -subj '/C=US/ST=Foo/L=Bar/O=Red Hat, Inc./CN=registry host certificate' \
      	-addext subjectAltName=DNS:localhost

.PHONY: start
start:
	@echo "Starting docker registry..."
	docker-compose --file registry/docker-compose.yml up --detach

.PHONY: stop
stop:
	@echo "Stopping docker registry..."
	docker-compose --file registry/docker-compose.yml stop

.PHONY: login
login:
	@echo "Logging in to docker registry..."
	$(SKOPEO) login $(REGISTRY) --username user --password password

.PHONY: install
install:
	@echo "Installing app dependencies..."
	pnpm install --dir ./app --frozen-lockfile

.PHONY: build
build: install
ifeq ($(OS), Windows_NT)
	@echo "Note that there is no umoci for Windows (yet). Did you mean to run build-within-docker?"
else
	@echo "Building app image..."
	skopeo copy docker://node:16-slim oci:$(IMAGE):$(TAG)
	umoci config --image $(IMAGE):$(TAG) --config.user "node:node"
	umoci config --image $(IMAGE):$(TAG) --config.cmd "node" --config.cmd "index.js"
	umoci config --image $(IMAGE):$(TAG) --config.workingdir "/home/node/app"
	umoci config --image $(IMAGE):$(TAG) --config.env "TZ=Europe/Berlin"
	umoci config --image $(IMAGE):$(TAG) --author "Olof Nord"
	umoci config --image $(IMAGE):$(TAG) --created "$(shell date --iso-8601=seconds)"
	umoci insert --rootless --image $(IMAGE):$(TAG) app /home/node/app
	umoci insert --rootless --image $(IMAGE):$(TAG) app/node_modules /home/node/app/node_modules
	mkdir -p build
	skopeo copy oci:$(IMAGE):$(TAG) dir:build/$(IMAGE)
endif

.PHONY: build-within-docker
build-within-docker:
	docker build -t umoci-pnpm-demo-builder .
ifeq ($(OS), Windows_NT)
	# docker cp does not work on Windows, instead use a volume
	docker run --rm --volume $(shell cmd /c cd):/home/rootless/tmp umoci-pnpm-demo-builder cp -r /home/rootless/build /home/rootless/tmp
else
	docker cp umoci-pnpm-demo-builder:/home/rootless/build .
endif

.PHONY: push
push:
ifeq ($(OS), Windows_NT)
	# skopeo tries to look for a /etc/containers/policy.json file unless ran with the insecure policy
	$(SKOPEO) --insecure-policy copy --format v2s2 dir:build/$(IMAGE) docker://$(REGISTRY)/$(IMAGE):$(TAG)
else
	$(SKOPEO) copy --format v2s2 dir:build/$(IMAGE) docker://$(REGISTRY)/$(IMAGE):$(TAG)
endif

.PHONY: run
run:
	@echo "Running app..."
	docker run -it --rm $(REGISTRY)/$(IMAGE):$(TAG)

.PHONY: debug
debug:
	@echo "Debugging app..."
	docker run -it --rm $(REGISTRY)/$(IMAGE):$(TAG) bash

.PHONY: pull
pull:
	@echo "Pulling image..."
	docker pull $(REGISTRY)/$(IMAGE):$(TAG)

.PHONY: inspect
inspect: pull
	@echo "Inspecting image..."
	docker inspect $(REGISTRY)/$(IMAGE):$(TAG)

.PHONY: clean
clean:
	@echo "Removing image and build files ..."
	rm -rf build
	$(SKOPEO) delete docker://$(REGISTRY)/$(IMAGE):$(TAG)
