FROM opensuse/leap:15.2

# This Dockerfile is used to build an image within a docker context,
# but actually without docker itself used for the image build

RUN zypper install -y --no-recommends umoci skopeo nodejs curl make
# https://pnpm.io/installation#nodejs-is-preinstalled
RUN curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm

RUN useradd -u 1000 -m -d /home/rootless -s /bin/bash rootless
USER rootless
WORKDIR /home/rootless

COPY --chown=rootless:users app/ /home/rootless/app
COPY --chown=rootless:users Makefile /home/rootless

RUN mkdir -p /home/rootless/build
RUN chown rootless:users /home/rootless/build

RUN make build
