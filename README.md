# umoci-pnpm-demo

Demo project for a rootless docker/OCI build using umoci and skopeo together with pnpm for dependency management.

Without using docker, and additionally without never needing root privileges, a docker image/OCI image is built.

The interesting parts of this project are basically all in the [Makefile](Makefile).

## Initialise

For the setup to be fully standalone, a local docker registry setup is used, see [registry](./registry).

```shell
make init
```

The initial setup of certificate generation and login is only needed once, on subsequent runs start is enough.

```shell
make start
```

## Build

This requires `umoci`, `skopeo`, `pnpm`, `openssl` and `make` to be installed locally.
It also assumes that the local docker registry is available.

```shell
make build push
```

## Build within a Docker environment

Some CI/CD environments, for example Gitlab CI, runs within Docker.

This can be tried out with the Dockerfile.

```shell
make build-within-docker push
```

## Run

Once the image is built, it can be ran using the Makefile as well:

```shell
make run
docker run -it --rm localhost:5000/umoci-pnpm-demo:latest
 ____________________________________
< umoci is a rootless OCI build tool >
 ------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
             U  ||----w |
                ||     ||
```

## All-in-one

For a fully automated workflow, the following four targets work well:

```shell
make clean build push run
```

## Windows

To use skopeo for Windows, [gaborcsardi](https://github.com/gaborcsardi/skopeo/releases) provides prebuilt binaries.

For skopeo to work with a self-signed certificate, it needs to be imported first.

Double-click on the domain.crt file, and choose install certificate.

When prompted, select the following options:

Store location: local machine

Place all certificates in the following store
Click Browse and select Trusted Root Certificate Authorities.

For full details, see [Docker: Use self-signed certificates](https://docs.docker.com/registry/insecure/#use-self-signed-certificates)

## Resources

* [Marc Brandner: Inspecting a Private Docker Container Registry](https://marcbrandner.com/blog/inspecting-a-private-docker-container-registry/)
